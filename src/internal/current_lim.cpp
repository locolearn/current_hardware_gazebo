///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2015, PAL Robotics S.L.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   * Neither the name of PAL Robotics S.L. nor the names of its
//     contributors may be used to endorse or promote products derived from
//     this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////////




#include <cassert>
#include <stdexcept>

#include <angles/angles.h>

#include <gazebo/physics/Model.hh>

#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <hardware_interface/internal/demangle_symbol.h>

#include<internal/current_lim.h>
#include <pal_hardware_interfaces/current_limit_interface.h>

namespace gazebo_ros_control
{

namespace internal
{

CurrentLim::CurrentLim()
:   curr_lim_(1.0),
    effort_lim_(0.0),
    is_init_(false)
{}

void CurrentLim::init(const std::string&           resource_name,
                         const ros::NodeHandle&       nh,
                         gazebo::physics::ModelPtr    gazebo_model,
                         const urdf::Model* const     urdf_model,
                         hardware_interface::RobotHW* robot_hw)
{
  name_ = resource_name;

  namespace hi  = hardware_interface;
  namespace hii = hi::internal;
  pal_ros_control::CurrentLimitInterface* curr_iface = robot_hw->get<pal_ros_control::CurrentLimitInterface>();
  if (!curr_iface)
  {
    const std::string msg = "Robot hardware abstraction does not have hardware interface '" +
                             hii::demangledTypeName<pal_ros_control::CurrentLimitInterface>() + "'.";
    throw std::runtime_error(msg);
  }

  // resource is already registered in hardware interface
  if (hasResource(getName(), *curr_iface))
  {
    throw ExistingResourceException();
  }

  pal_ros_control::CurrentLimitHandle curr_handle(getName(), &curr_lim_);
  curr_iface->registerHandle(curr_handle);
  sim_joint_ = gazebo_model->GetJoint(resource_name);
}

void CurrentLim::read(const ros::Time&     time,
                          const ros::Duration& period,
                          bool                 in_estop)
{
  double limit = sim_joint_->GetEffortLimit(0u);
  if (!is_init_) {
      is_init_ = true;
      effort_lim_ = limit;
  }
  curr_lim_ = limit/effort_lim_;
}
void CurrentLim::write(const ros::Time&     time,
                          const ros::Duration& period,
                          bool                 in_estop)
{
  sim_joint_->SetEffortLimit(0u, effort_lim_ *  curr_lim_);
}

std::vector<std::string> CurrentLim::getHardwareInterfaceTypes()
{
  namespace hi  = hardware_interface;
  namespace hii = hi::internal;

  std::vector<std::string> out;
  out.push_back(hii::demangledTypeName<pal_ros_control::CurrentLimitInterface>());
  return out;
}

} // namespace

} // namespace
