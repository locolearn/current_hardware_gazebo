///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2016, PAL Robotics S.L.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   * Neither the name of PAL Robotics S.L. nor the names of its
//     contributors may be used to endorse or promote products derived from
//     this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <boost/foreach.hpp>

#include <gazebo/sensors/SensorManager.hh>

#include <urdf_parser/urdf_parser.h>
#include <pluginlib/class_list_macros.h>
#include <angles/angles.h>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <joint_limits_interface/joint_limits_urdf.h>
#include <transmission_interface/transmission_interface_loader.h>

#include <current_hardware_gazebo/current_hw_gazebo.h>
#include <pal_hardware_gazebo/pal_hardware_gazebo.h>

#include <dynamic_introspection/dynamic_introspection.h>
#include <gazebo/gazebo_config.h>


using std::vector;
using std::string;


namespace gazebo_ros_control
{
using namespace hardware_interface;


CurrentHardwareGazebo::CurrentHardwareGazebo() : PalHardwareGazebo()
{
}

bool CurrentHardwareGazebo::initSim(const std::string& robot_ns, ros::NodeHandle nh,
                                gazebo::physics::ModelPtr model,
                                const urdf::Model* const urdf_model,
                                std::vector<transmission_interface::TransmissionInfo> transmissions)
{
  ROS_INFO_STREAM("Loading CURRENT HARWARE GAZEBO");
  
  registerInterface(&cl_interface_);
  // cache transmisions information
  transmission_infos_ = transmissions;

  // populate hardware interfaces, bind them to raw Gazebo data
  namespace ti = transmission_interface;
  BOOST_FOREACH (const ti::TransmissionInfo& tr_info, transmission_infos_)
  {
    BOOST_FOREACH (const ti::JointInfo& joint_info, tr_info.joints_)
    {
      BOOST_FOREACH (const std::string& iface_type, joint_info.hardware_interfaces_)
      {
        // TODO: Wrap in method for brevity?
        RwResPtr res;
        boost::shared_ptr<internal::CurrentLim> res_current;
        // TODO: A plugin-based approach would do better than this 'if-elseif' chain
        // To do this, move contructor logic to init method, and unify signature
        if (iface_type == "hardware_interface/JointStateInterface")
        {
          res.reset(new internal::JointState());
        }
        else if (iface_type == "hardware_interface/PositionJointInterface")
        {
          res.reset(new internal::PositionJoint());
        }
        else if (iface_type == "hardware_interface/VelocityJointInterface")
        {
          res.reset(new internal::VelocityJoint());
        }
        else if (iface_type == "hardware_interface/EffortJointInterface")
        {
          res.reset(new internal::EffortJoint());
        }

        res_current.reset(new internal::CurrentLim());
        if(res && res_current)
        {
          try{
            res_current->init(joint_info.name_, nh, model, urdf_model, this);
            rw_resources_.push_back(res_current);
            default_active_resources_[res_current->getName()] = res_current;
            ROS_DEBUG_STREAM("Registered joint '" << joint_info.name_
                                                  << "' in hardware interface '"
                                                  << "pal_ros_control/CurrentLimitInterface" << "'.");
            const string ns = "/current_hardware_gazebo";
          }
          catch (const internal::ExistingResourceException&)
          {
            ROS_DEBUG_STREAM("Ressource already added, we do not add it again in default_active_resources_\n");
          }  // resource already added, no problem
          catch (const std::runtime_error& ex)
          {
            ROS_ERROR_STREAM("Failed to initialize gazebo_ros_control plugin.\n"
                            << ex.what());
            return false;
          }
          catch (...)
          {
            ROS_ERROR_STREAM("Failed to initialize gazebo_ros_control plugin.\n"
                            << "Could not add resource '" << joint_info.name_
                            << "' to hardware interface '" << "CurrentLimitInterface" << "'.");
            return false;
          }
        }
      }
    }
  }

  if (!PalHardwareGazebo::initSim(robot_ns, nh, model, urdf_model, transmissions))
  {
    ROS_INFO_STREAM("false");
    return false;
  }

  return true;
}

void CurrentHardwareGazebo::readSim(ros::Time time, ros::Duration period)
{
  
  PalHardwareGazebo::readSim(time, period);

}

void CurrentHardwareGazebo::writeSim(ros::Time time, ros::Duration period)
{

  PalHardwareGazebo::writeSim(time, period);
  
}
}

PLUGINLIB_EXPORT_CLASS(gazebo_ros_control::CurrentHardwareGazebo, gazebo_ros_control::RobotHWSim)
