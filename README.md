# current_hardware_gazebo
**WARNING: we limit effort instead of current!!**
**Even if we use an approximated motor torque constant, the real robot behavior might be different !!**


* This extends the pal gazebo ros_control plugin (pal_hardware_controller) with a current hardware interface for gazebo
* It is now possible to use the pal current_limit_controller with gazebo

## Install

```
cd ~/catkin_ws/src
git clone https://github.com/pal-robotics/pal_hardware_gazebo.git
cd ~
git clone https://github.com/pal-robotics-forks/gazebo_ros_pkgs
cd gazebo_ros_pkgs
git checkout ferrum-devel
cp -R gazebo_ros_control/ ~/catkin_ws/src/
cd ~/catkin_ws/src 
git clone https://gitlab.inria.fr/locolearn/current_hardware_gazebo.git
cd ~/catkin_ws
catkin_make 
```

## Based on

It adds a current gazebo hardware interface to:

https://github.com/pal-robotics/pal_hardware_gazebo.git


Also see gazebo_ros_control forked by pal here:

https://github.com/pal-robotics-forks/gazebo_ros_pkgs


## How to use ?

```
cd /opt/pal/ferrum/share/talos_description/gazebo
sudo cp gazebo.urdf.xacro gazebo_save.urdf.xacro
sudo cp ~/catkin_ws/src/current_hardware_gazebo/utils/gazebo.urdf.xacro /opt/pal/ferrum/share/talos_description/gazebo/gazebo.urdf.xacro
```
* Start the simulation with current_limit_controllers
```
roslaunch current_hardware_gazebo  talos_gazebo_current.launch
```

* Start your custom controller ex:

```
roslaunch talos_controller talos_controller.launch
```

* Then either set the limits from the gui : 

```
roslaunch talos_rqt_tutorials talos_gui.launch 
```

* Or from command line ex:
```
rostopic pub /left_arm_current_limit_controller/command pal_control_msgs/ActuatorCurrentLimit "{actuator_names:['arm_left_1_motor','arm_left_2_motor','arm_left_3_motor','arm_left_4_motor','arm_left_5_motor','arm_left_6_motor','arm_left_7_motor'],current_limits:[0,0,0,0,0,0,0]}"
```
## More info
```
roslaunch current_hardware_gazebo  talos_gazebo_current.launch
```
Is equivalent to
```
roslaunch current_hardware_gazebo  motor_torque_constant.launch
roslaunch current talos_gazebo.launch
roslaunch talos_controller_configuration current_limit_controllers.launch
```
