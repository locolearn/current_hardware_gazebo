#ifndef CURRENT_HW_GAZEBO_H
#define CURRENT_HW_GAZEBO_H

#include <vector>
#include <string>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <gazebo_ros_control/robot_hw_sim.h>
#include <gazebo/physics/physics.hh>
#include <internal/current_lim.h>
#include <pal_hardware_gazebo/pal_hardware_gazebo.h>

typedef Eigen::Isometry3d eMatrixHom;

namespace gazebo_ros_control
{


class CurrentHardwareGazebo : public PalHardwareGazebo
{
public:

  CurrentHardwareGazebo();

  // Simulation-specific
  virtual bool initSim(const std::string& robot_ns,
               ros::NodeHandle nh,
               gazebo::physics::ModelPtr model,
               const urdf::Model* const urdf_model,
               std::vector<transmission_interface::TransmissionInfo> transmissions);
  virtual void readSim(ros::Time time, ros::Duration period);
  virtual void writeSim(ros::Time time, ros::Duration period);

private:
  pal_ros_control::CurrentLimitInterface cl_interface_;

};

}

#endif // current_hw_gazebo_H
