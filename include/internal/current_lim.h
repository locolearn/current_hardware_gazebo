///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2015, PAL Robotics S.L.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   * Neither the name of PAL Robotics S.L. nor the names of its
//     contributors may be used to endorse or promote products derived from
//     this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////////

#ifndef _GAZEBO_ROS_CONTROL__INTERNAL__CURRENT_LIM_H_
#define _GAZEBO_ROS_CONTROL__INTERNAL__CURRENT_LIM_H_

#include <gazebo_ros_control/internal/read_write_resource.h>
#include <gazebo_ros_control/internal/joint_state.h>
#include <pal_hardware_interfaces/current_limit_interface.h>

namespace gazebo_ros_control
{

namespace internal
{

  
class CurrentLim : public ReadWriteResource
{
public:
  CurrentLim();

  virtual void init(const std::string&                        resource_name,
                    const ros::NodeHandle&                    nh,
                    boost::shared_ptr<gazebo::physics::Model> gazebo_model,
                    const urdf::Model* const                  urdf_model,
                    hardware_interface::RobotHW*              robot_hw);

  virtual void read(const ros::Time&     time,
                     const ros::Duration& period,
                     bool                 in_estop);
  virtual void write(const ros::Time&     time,
                     const ros::Duration& period,
                     bool                 in_estop);

  virtual std::vector<std::string> getHardwareInterfaceTypes();

  virtual std::string getName() const {return name_.substr(0, name_.size()-5)+"motor";}

protected:
  std::string name_;
  gazebo::physics::JointPtr sim_joint_;
  float effort_lim_;
  double curr_lim_;
  bool is_init_;

};

} // namespace

} // namespace

#endif
